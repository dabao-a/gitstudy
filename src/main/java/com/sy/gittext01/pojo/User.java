package com.sy.gittext01.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author:孙勇
 * @Description: 用户实体类
 * @Date:2022/1/2 10:15
 * @Modified:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class User {
    //设置id自增,雪花算法
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    private String name;
    private Integer age;
    private String email;
    //声明乐观锁
    @Version
    private Integer version;
    //开启逻辑删除
    @TableLogic
    private Integer deleted;
    //字段添加填充内容
    //创建时自动插入
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    //更新或创建时自动更新
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
