package com.sy.gittext01;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitText01Application {

    public static void main(String[] args) {
        SpringApplication.run(GitText01Application.class, args);
    }

}
