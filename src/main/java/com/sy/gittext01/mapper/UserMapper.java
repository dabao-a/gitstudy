package com.sy.gittext01.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.sy.gittext01.pojo.User;

import org.springframework.stereotype.Repository;

/**
 * @Author: 大宝
 * @Description:  用户实现类接口
 * @Date:2022/1/2 10:18
 * @Modified: 持久层对象
 */
@Repository
public interface UserMapper extends BaseMapper<User> {



}
