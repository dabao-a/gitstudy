package com.sy.gittext01;


import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import java.util.Collections;


/**
 * @Author:孙勇
 * @Description:  代码生成器类
 * @Date:2022/1/2 18:09
 * @Modified:
 */

public class MyAutoGenerator {

    public static void main(String [] args) {
        FastAutoGenerator.create("jdbc:p6spy:mysql://localhost:3306/text?useSSL=false&useUnicode=true&characterEncoding=utf-8"
                , "root", "yong")
                .globalConfig(builder -> {
                    builder.author("大宝") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("D:/Gitmodule/git-text01/src/main/java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.sy.gittext01") // 设置父包名
                            .moduleName("system") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml,
                                    "D:/Gitmodule/git-text01/src/main/resources/mybatis/mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("dept") // 设置需要生成的表名
                            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }

}
