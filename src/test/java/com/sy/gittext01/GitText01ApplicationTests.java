package com.sy.gittext01;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sy.gittext01.mapper.UserMapper;
import com.sy.gittext01.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class GitText01ApplicationTests {

    @Autowired
    private UserMapper userMapper;
    //测试查询
    @Test
    void contextLoads() {
        List<User> list = userMapper.selectList(null);
        list.forEach(System.out::println);
    }
    //测试插入
    @Test
    void text01(){
        User user=new User();
        user.setName("小王学java").setAge(13).setEmail("12131@qq.com");
        int insert = userMapper.insert(user);
        System.out.println(insert);
    }
    //测试修改
    @Test
    void text02(){
        User user=new User();
        user.setName("李四说刑法").setAge(14).setEmail("12131@qq.com").setId(6L);
        int update = userMapper.updateById(user);//返回受影响行数
        System.out.println(update);
    }
    //单线程测试乐观锁
    @Test
    void text03(){
        //查询用户
        User user=userMapper.selectById(5L);
        //修改信息
        user.setName("张三学php").setAge(12).setEmail("12131@qq.com");
        //执行操作
        int update = userMapper.updateById(user);//返回受影响行数
        System.out.println(update);
    }
    //模拟多线程测试乐观锁
    @Test
    void text04(){
        //模拟线程一号
        User user=userMapper.selectById(5L);
        user.setName("张三学c++").setAge(11).setEmail("12131@qq.com");

        //模拟线程二号插队操作
        User user2=userMapper.selectById(5L);
        user2.setName("张三学Android").setAge(12).setEmail("12131@qq.com");
        userMapper.updateById(user2);

        userMapper.updateById(user);
    }
    //测试查询操作
    @Test
    void text05(){
        userMapper.selectBatchIds(Arrays.asList(1, 2, 3)).forEach(users-> System.out.println(users));
    }
    //测试条件查询map
    @Test
    void text06(){
        Map<String,Object> map=new HashMap<>();
        map.put("name","李四说刑法");
        map.put("age",14);
        userMapper.selectByMap(map).forEach(System.out::println);
    }
    //测试分页查询
    @Test
    void text07(){

        Page<User> page =new Page<>(1,5);
        userMapper.selectPage(page,null);
        page.getRecords().forEach(System.out::println);

    }
    //测试删除
    @Test
    void text08(){
        userMapper.deleteById(3L);
        //userMapper.deleteBatchIds(Arrays.asList(1477475583440003073L,1477473211745673218L));
    }
    //测试删除map
    @Test
    void text09(){
        Map<String,Object> map=new HashMap<>();
        map.put("id","1477475356435902466");
        map.put("name","张三学刑法");
        userMapper.deleteByMap(map);
    }
    //复杂类型测试查询
    @Test
    void selectText() {
        //查询年龄大于等于14,小于18,邮箱不为空，且未被删除的用户,结果集按id降序排列
        QueryWrapper<User> wrapper=new QueryWrapper();
        wrapper
                .eq("deleted",0)
                .ge("age",14)
                .isNotNull("email")
                .orderByDesc("id")
                .and(i->i.le("age",18));

        userMapper.selectList(wrapper).forEach(System.out::println);
    }

}
